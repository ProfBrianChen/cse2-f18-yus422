/* Yuhan Su 
   CSE002 Section210 
   October 30th*/



import java.util.Scanner;
public class WordTools{
  //main method required for every java program
  
  public static String sampleText() {
    Scanner scr = new Scanner(System.in); //Scanner method
    System.out.println("Enter a sample text: ");
    String userEntered = scr.nextLine(); 

    System.out.println();
    System.out.println("You entered: " + userEntered);
    return userEntered; //return to main method
    }

  public static char printMenu() {
    Scanner scr = new Scanner(System.in);
    char ch = scr.next().charAt(0); //enter a number using the char variable
    return ch;   
  }
  
  public static int getNumOfNonWSCharacters(String snr) { //the first method corresponding to number c
    int num = 0;
    for ( int i = 0; i <= snr.length()-1; i++) {
      if (snr.charAt(i) != ' ' )  
        num++; 
    }
    return num;   
  }


public static int getNumOfWords(String str) { //the method corresponding to number w
int numWord = 0;
int i; //loop, from location 0, till (str.length()-1)
boolean isBlank= true; //add boolean variable to indicate it is not a space

 for ( i = 0; i <= str.length()-1; i++) { //find first character that is not a space
   if (str.charAt(i) != ' ')  { //if the character on the i location is not a space
     isBlank = false;
   }
 else {//if it is a space
   if (isBlank == false)//  check if the prior one is blank or not
   numWord++; //the number of word increases
   isBlank = true; // change it to true
 }//else ends 
}
 
if (isBlank == false); //check end of loop is blank or not
 numWord = numWord + 1; //if not blank, increases by one, or else does not change
 return numWord;   
}

public static int findText(String sub, String totalStr) { //the method corresponding to number f
  int num = 0; //initial num to 0
  int i;  
  i = totalStr.indexOf(sub);
  while ( i >= 0)  { 
  num++;  
  totalStr= totalStr.substring(i+sub.length(),totalStr.length()); 
  i = totalStr.indexOf(sub); 
  }
  return num;
}

public static String replaceExclamation (String str) { //the method corresponding to number r
  String newstr;
  newstr = str.replace('!', '.');
  return newstr;
}

public static String shortenSpace(String str) { //the method corresponding to number s
  String oldString = "\\s";
  String newString = "";
  str = str.replaceAll(oldString, newString);
  return str;
}


  public static void main(String[] args){ //the main method
    String userEntered;
    userEntered = sampleText(); //the first step
    char ch;
    ch = printMenu();
  
    int num;
    String myStr;
    while ( ch != 'q') { 
      if ((ch == 'c')||(ch == 'w') ||(ch == 'f')||(ch == 'r')||(ch == 's')) { 
        switch (ch) {
          case 'c' :
            System.out.print("Number of non-whitespace characters:");
            num = getNumOfNonWSCharacters(userEntered);
            System.out.print(num);
            System.out.println();
            break;
          case 'w':
            System.out.print("Number of words: ");
            num = getNumOfWords(userEntered);
            System.out.print(num);
            System.out.println();
            break;
          case 'f' :
            Scanner scr = new Scanner(System.in);
            System.out.print("Find text: ");
            myStr = scr.nextLine();
            System.out.print("\"myStr\" " + "instances:");  
            num = findText(myStr,userEntered);
            System.out.print(num);
            System.out.println();
            break;
          case 'r' :
            System.out.print("Replace all !'s:  ");
            myStr = replaceExclamation(userEntered);
            System.out.print(myStr);
            System.out.println();
            break;
            case 's' :
            System.out.print("Shorten spaces: ");
            myStr = shortenSpace(userEntered);
            System.out.print(myStr);
            System.out.println();
            break;
          } 
        }
      else{
        System.out.println("Error In Value");
      }
        ch = printMenu(); //do this or else it will be a infinite loop
 } 
      return;

  } //end of main method
} //end of class