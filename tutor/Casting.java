public class Casting{
  public static void main(String args []){
    int myInteger, myInteger2;
    double myDouble = 5.6;
    double result;
    myInteger = 5;
    myInteger2 = 3;
    
    myDouble = 3/2;
    System.out.println("myDouble " + myDouble);
    
    //loses decimal value
    result = myInteger / myInteger2;
    System.out.println("Result: " + result);
    
    //loses decimal value with casting
    result = (double)(myInteger / myInteger2);
    System.out.println("Casting Result: " + result);
    
    // by changing one int to a double 
    result = (double)myInteger / myInteger2;
    System.out.println("Casting Result 2: " + result);
    
    //loses decimal place when cast to int
     myInteger = (int)myDouble;
    System.out.println("double to int: " + myInteger);
    
    //will not round up
    
    myDouble = 5.5;
    myInteger = (int) (myDouble + 11.6);
    
    // will not round up
    myInteger = (int) myDouble +  (int) myDouble;
    
    System.out.println("double to int expression " + myInteger);
    //increments myInteger2 by 1
    myInteger2++;
    //increments myInteger2 by 1
    ++myInteger2;
    
    // sets myInteger to myInteger and then increments myInteger2
    myInteger  = myInteger2++;
    
    System.out.println("double to int expression " + myInteger + " " + myInteger2);
    
    //increments myInteger2 by one and then set myInteger to myInteger2's new value
    myInteger  = ++myInteger2;
    myInteger = myInteger2++;
    
     System.out.println("double to int expression " + myInteger + " " + myInteger2);
    myInteger = 5;
    myInteger2 = 10;
    
    System.out.println("results " + (myInteger++  + myInteger2));
    System.out.println("results " + (myInteger++  + ++myInteger2));
    System.out.println(myInteger++ + (double) myInteger2/myInteger);
   
    
    
    
  }
}