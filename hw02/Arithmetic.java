/* Yuhan Su 
   September 11 
   CSE002-210
   The purpose of hw02 is to let me practice manipulating data, running simple calculations and in printing the numerical output that I generated.
*/
  
public class Arithmetic {
  
  public static void main(String args[]) {
   //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //total cost of pants
    double totalCostOfPants = numPants * pantsPrice;
    //total cost of shirts
    double totalCostOfShirts = numShirts * shirtPrice;
    //total cost of belts
    double totalCostOfBelts = numBelts * beltCost;
    
    /*have only two digits appear to the right of the decimal point, which looks a lot nicer
      print total cost of pants, shirts, and belts */
    System.out.printf("Total cost of pants was %.2f dollars\n", totalCostOfPants); 
    System.out.printf("Total cost of shirts was %.2f dollars\n", totalCostOfShirts);
    System.out.printf("Total cost of belts was %.2f dollars\n", totalCostOfBelts);
    
    //sales tax charged on pants
    double salesTaxOnPants = totalCostOfPants * paSalesTax;
    //sales tax charged on shirts
    double salesTaxOnShirts = totalCostOfShirts * paSalesTax;
    //sales tax charged on belts
    double salesTaxOnBelts = totalCostOfBelts * paSalesTax;
    
    
    /*have only two digits appear to the right of the decimal point, which looks a lot nicer
      print sales tax charged on pants, shirts, and belts */
    System.out.printf("Sales tax charged on pants was %.2f dollars\n", salesTaxOnPants);
    System.out.printf("Sales tax charged on shirts was %.2f dollars\n", salesTaxOnShirts);
    System.out.printf("Sales tax charged on belts was %.2f dollars\n", salesTaxOnBelts);


    //Total cost of purchases before tax
    double totalCostOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    
    /*have only two digits appear to the right of the decimal point, which looks a lot nicer
      print the total cost of purchases before tax */
    System.out.printf("Total cost of purchases before tax was %.2f dollars\n", totalCostOfPurchase);

    //Total sales tax
    double totalSalesTax = totalCostOfPurchase * paSalesTax;
    
   /*have only two digits appear to the right of the decimal point, which looks a lot nicer
      print the total sales tax */
    System.out.printf("Total sales tax was %.2f dollars\n", totalSalesTax);


    //Total paid for this transaction including sales tax
    double totalTransaction = totalCostOfPurchase + totalSalesTax;
    
    /*have only two digits appear to the right of the decimal point, which looks a lot nicer
      print the total transaction cost including sales tax */
    System.out.printf("Total paid for this transaction including sales tax was %.2f dollars\n", totalTransaction);

/* I'm not sure if I should add bio to hw02, because it's not listed in the hw description. 
   Anyways I'm Yuhan Su, majoring in CSB. I love playing the piano. */
    
  }
}