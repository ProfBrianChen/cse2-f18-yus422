/* Yuhan Su 
   CSE002 Section210 
   October 11th*/

//The purpose of this homework is to get familiar with loops, a critical piece of syntax that is essential for many programming languages.  The program you will write will print out a simple twist on the screen.

import java.util.Scanner;
  public class TwistGenerator{
  //main method required for every java program
  public static void main (String[]args){
    
    Scanner scr = new Scanner(System.in);
     
    System.out.println("Enter a positive integer: ");  //Ask the user for a positive integer
    int length = scr.nextInt(); 
    String junk;
    
   
      while ((length <= 0)&&(!scr.hasNextInt())){      
        System.out.println("Error in value");
        junk = scr.nextLine();
        length = scr.nextInt(); //If the user does not provide an integer, or if the integer is not positive, then ask again, inside a while loop
      }

      int i = 0;
      while (i < length / 3){ //How many sets of \\ / are printed out is determined by the length divided by three     
        System.out.print("\\ /");
        i++;
      } //Use the while loop to print out sets of \\ /
      switch (length % 3){ //What left to be printed out is determined by the length moduled by three
        case 1:
          System.out.println("\\");  //Use println because has to go to the next line after printed
          break;
        case 2:
          System.out.println("\\ ");
          break;
        case 0:
          System.out.println("");
          break;
      }
     
      int m = 0;
      while (m <length / 3){   //How many sets of  X  are printed out is determined by the length divided by three     
        System.out.print(" X ");
        m++;
      }
       switch (length % 3){ //What left to be printed out is determined by the length moduled by three
        case 1:
          System.out.println(" ");  //Use println because has to go to the next line after printed
          break;
        case 2:
          System.out.println(" X");
          break;
        case 0:
          System.out.println("");
          break;
      }
       
      int n = 0;
      while (n<length/3){  //How many sets of / \\ are printed out is determined by the length divided by three 
        System.out.print("/ \\");
        n++;
      }
      switch (length%3){   ////What left to be printed out is determined by the length moduled by three
        case 1:
          System.out.println("/");
          break;
        case 2:
          System.out.println("/ ");
          break;
        case 0:
          System.out.println("");
          break;
      }
     
  } //end of main method
} //end of class