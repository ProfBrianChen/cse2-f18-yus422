/* Yuhan Su(Melody)
   CSE002 Section 210
   September 5th */

public class Cyclometer{
  // main method required for every java programm
  public static void main (String [] args) {
    int secsTrip1 = 480; //number of seconds for trip 1 is 480 
    int secsTrip2 = 3220; //number of seconds for trip 2 is 3220
    int countsTrip1 = 1561; //number of counts for trip 1 is 1561
    int countsTrip2 = 9037; //number of counts for trip 2 is 9037
    
    double wheelDiameter = 27.0, // the diameter of the wheel is 27.0
    PI = 3.14159, //ratio of the circle's circumference to its diameter
    feetPerMile = 5280, //how many feet per mile it travles
    inchesPerFoot = 12, //how many foot are in one inch
    secondsPerMinute = 60; //how many seconds are in one minute
    double distanceTrip1, distanceTrip2, totalDistance; 
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) +" minutes and had " + countsTrip1+" counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) +" minutes and had " + countsTrip2+" counts.");
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    /*Above gives distance in inches
      (for each count, a rotation of the wheel travels
      the diameter in inches times PI)
      */
    distanceTrip1/=inchesPerFoot * feetPerMile; //gives distance
    distanceTrip2 = countsTrip2 * wheelDiameter * PI/inchesPerFoot/feetPerMile; //gives distance
    totalDistance = distanceTrip1 + distanceTrip2; //total distanceTrip1 and distanceTrip2 together
    
    //print out the output data.
    System.out.println ("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println ("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
    
  } //end of main method
} //end of class
  
