/* Yuhan Su 
   CSE002 Section210 
   September 19th*/


/*Generate a random number, create two String variables: a String corresponding to the name of the suit and a String corresponding to the identity of the card.
Use if statements to assign the suit name.
Use a switch statement to assign the card identity.
Print out the name of the randomly selected card.
*/

import java.util.*;
public class CardGenerator{
  //main method required for every java program
  public static void main (String[]args){


    int card = (int)(Math.random()*51)+1;//generate a random number

   
    
   String suit= "";
   String identity = "";
   //create two String variables
    
    
    if ((card >0)&&(card<=13)) {    //Use if statements to assign the suit name.

         suit = "Diamond";//Cards 1-13 represent the diamonds;
      }
      else if ((card>13)&&(card<=26)){
         suit = "Clubs"; //Cards 14-26 represent the clubs;
      }
      else if ((card>26)&&(card<=39)){
         suit = "Hearts"; //Cards 27-39 represent the hearts;
      }
      else{
         suit = "Spades";//Cards 40-52 represent the spades;
      }
    

    switch (card%13) { //Use a switch statement to assign the card identity.
       
      case 1: 
      identity = "ace";
      break; //when the card is 1,14,27,40, the identity is ace
      
      case 2: 
      identity = "2";
      break;
        
      case 3: 
      identity = "3";
      break;
        
      case 4: 
      identity = "4";
      break;
      
      case 5: 
      identity = "5";
      break;
        
      case 6: 
      identity = "6";
      break;
        
      case 7: 
      identity = "7";
      break; 
        
      case 8: 
      identity = "8";
      break;
        
      case 9: 
      identity = "9";
      break;
      
      case 10: 
      identity = "10";
      break;
        
      case 11:
      identity = "Jack";
      break; //when the card is 11,24,37,50, the identity is jack
   
      case 12:
      identity = "Queen";
      break; //when the card is 12,25,38,51, the identity is queen

      case 0:
      identity = "King"; //when the card is 13,26,39,52, the identity is king
      break; //Switch statements have a default feature that serves a similar purpose to else in an if statement.
      default:
      System.out.println("Unknown");
      break;
    }
      System.out.println("You picked the " + identity + " of " + suit); //Print out the name of the randomly selected card.

    
  } //end of main method
} //end of class