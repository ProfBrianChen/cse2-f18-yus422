/* Yuhan Su
 * CSE 002 Section 210
 * December 4th 
 * */
import java.util.Scanner;
public class HW10{
  public static void main(String[] args){
    int result;
    String [][] m = {{" 1 ", " 2 ", " 3 "},
      {" 4 ", " 5 ", " 6 "},
      {" 7 ", " 8 ", " 9 "}}; //declare an multidimensional array
    System.out.println("Display the board: ");
    String[] tokens = {" X "," O "}; //in order for the two players to take turns, declare the two tokens to indicate the player
    displayBoard(m);//display the board with numbers filled
    
    result = gameStatus(m, tokens[0]);     // This step determines game status
    displayBoard(m); 
    
    while(result == 2){ // If status is continue make next player take turn
       swap(tokens); //use the swap method to swap players
       result = gameStatus(m, tokens[0]);
       displayBoard(m);
    }

    if (result == 0)  // Display game results
      System.out.println(tokens[0] + "player won"); 
    else
      System.out.println("Players draw");
  }//end of main method
  
  public static void displayBoard(String [][] m){
      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
        System.out.print(m[i][j]); //printing out the board
        }
      System.out.println();
      }
      return;
  } //end of displayBoard method
  
  public static void setToken(String[][] m, String t) {
    int userInput;
    do{
      userInput = getInput(m); //what getInput method returns
    }while(userInput == 0);
     int i = (userInput - 1) / 3;
     int j = (userInput - 1) % 3;
     m[i][j] = t;//change the element to the token when displaying the board
     return;
  }//end of setToken method
    
  public static int getInput(String[][]m){
    int userInput;
    System.out.println("Where do you want to place your token? (Enter an integer from 1 to 9)");
    Scanner scr = new Scanner(System.in); //Create a Scanner
   if (!scr.hasNextInt()){ //input is not an integer
     System.out.println("Error not an integer");
     return 0;
   }
    userInput = scr.nextInt(); // Prompt player to enter a token
    if ((userInput <= 0)||(userInput >= 10)) { //input is out of range
      System.out.println("Invalid cell");
      return 0;
    }
    int i = (userInput - 1) / 3;
    int j = (userInput - 1) % 3;
    if (m[i][j] == " O " || m[i][j] == " X "){ //if input is the same the input last player placed, then it is filled
      System.out.println("The position is already filled.");
      return 0;
    }
    return userInput; //return the integer
  }//end of getInput method
  

 public static int gameStatus(String[][] m, String e) {   //this method determines the status of the game 
  setToken(m, e);  
  if (isWin(m, e))
    return 0; // status is Win
  else if (isDraw(m))
    return 1; // status is Draw
  else
    return 2; // status is Continue
 } //end of gameStatus method

 public static boolean isWin(String[][] m, String t) {  //This method returns true if one player has placed three tokens in a horizontal, vertical, or diagonal line on the board 
   return isHorizontalWin(m, t) || isVerticalWin(m, t) || isDiagonalWin(m, t);
 } //end of isWin method

 public static boolean isHorizontalWin(String[][] m, String t) { //this method returns true if player has placed three tokens in a horizontal line 
  for (int i = 0; i < m.length; i++) {
    int count = 0;
    for (int j = 0; j < m[i].length; j++) {
      if (m[i][j] == t)
        count++;
    }
    if (count == 3)
      return true;
  }
  return false;
 }//end of isHorizontalWin method

 public static boolean isVerticalWin(String[][] m, String t) { //this method returns true if player has placed three tokens in a vertical line 
  for (int i = 0; i < m.length; i++) {
    int count = 0;
    for (int j = 0; j < m[i].length; j++) {
      if (m[j][i] == t)
        count++;
    }
    if (count == 3)
      return true;
  }
  return false;
 }//end of isVerticalWin method

 public static boolean isDiagonalWin(String[][] m, String t) { //this method returns true if player has placed three tokens in a diagonal line 
  int count = 0;
  for (int i = 0; i < m.length; i++) {
   if (m[i][i] == t) 
    count++;
   if (count == 3) //if the token has three on the same line
    return true;
  }//end of isDiagonalWin method

  count = 0;
  for (int i = 0, j = m[i].length - 1; j >= 0 ; j--, i++) {
   if (m[i][j] == t)
    count++;
   if (count == 3)
    return true;
  }
  return false;
 } // End of isDiagonalWin method

 public static boolean isDraw(String[][] m) { //This method returns true if all the cells on the grid have been filled with tokens and neither player won 
   int count = 0;
   for (int i = 0; i < m.length; i++) {
     for (int j = 0; j < m[i].length; j++) {
       if (m[i][j] == " O "||m[i][j] == " X ")
         count ++;
     }
   }
   if (count == 9) //if the 9 elements on the board is filled
    return true;
    else
    return false;
 } //end of isDraw method

 public static void swap(String[] p) { //this method swaps the two elements in the array, so as the players
   String temp = p[0];
   p[0] = p[1];
   p[1] = temp;
 } //end of swap method

}//end of class

      