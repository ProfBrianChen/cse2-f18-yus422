/* Yuhan Su 
   CSE002 Section210 
   October 23rd*/



import java.util.Scanner;
public class EncryptedX{
  //main method required for every java program
  public static void main (String[]args){
    
    Scanner scr = new Scanner(System.in);
    
    System.out.println("Enter a integer between 1 and 100: ");  //Ask the user for a positive integer between 1 and 100 
    int Input = scr.nextInt(); 
    String junk;
    int i;
    int j;
    int k;
    
    

    while((Input <= 0)||(Input >= 101)&&(!scr.hasNextInt())){ //Validate input
      System.out.println("Error In Value");
      junk = scr.nextLine();
      Input = scr.nextInt();
    }
      System.out.println("num user entered = " + Input);

//This is the first part of the whole program, I divided it into three
      for(i = 1; i <= Input / 2; i++){ //First for loop, repeat input/2 times
        for(j = 1; j <= Input + 1; j++){ //Second for loop, repeat input+1 times, print out input+1 characters in total
          if(j == i) //prints out characters as follow
            System.out.print(" ");
          else if(j == Input + 2 - i)
            System.out.print(" ");
          else
            System.out.print("*");
        }
        System.out.println("");
      }
      
	  //This is the second part of the whole program
      for(i = 1; i <= Input + 1; i++){ //The for loop letting it repeat input+1 times, so that it prints out input+1 characters in total
        if(Input % 2 == 0){//It differentiates between odd and even numbers, so this if statement works for even numbers
          if(i == Input / 2 + 1)
            System.out.print(" ");
          else
            System.out.print("*");
        }
        else if(Input % 2 == 1){ //This if statement works for odd numbers
          if(i == Input / 2 + 1)
            System.out.print(" ");
          else if(i == Input / 2 + 2)
            System.out.print(" ");
          else
            System.out.print("*")
        }
      }
      System.out.println("")
  
  //This is the third part of the whole pragram, which is simply the opposite of the first part
      for(i = Input / 2; i >= 1; i--){ //The for statement is the opposite of the first one, but the rest stays the same
        for(j = 1; j <= Input + 1; j++){ //The same as part one, prints out characters as follows
          if(j == i)
            System.out.print(" ");
          else if(j == Input + 2 - i)
            System.out.print(" ");
          else
            System.out.print("*");
        }
        System.out.println("");
      }
  } //end of main method
} //end of class