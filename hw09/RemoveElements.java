import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
 int num [] = new int[10];
 int newArray1[];
 int newArray2[];
 int index,target;
 String answer = "";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   System.out.println();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+= listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
   System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
    String out="{";
    for(int j = 0;j < num.length;j++){
      if(j > 0){
        out += ", ";
      }
      out += num[j];
    }
    out += "} ";
    return out;
  }
  
  
  public static int [] randomInput(){
    int array[] = new int[10]; //the array has 10 elements
    for (int i = 0; i < array.length; i++) { 
      array[i] = (int)(Math.random() * 9);
      System.out.print(array[i] + " ");
    }
    return array; //return array to main method
  }
  
  public static int [] delete(int [] list, int pos){ // should create a new array that has one member fewer than list and be composed of all of the same members except the member in the position pos 
    if ((pos < 0) ||(pos > 9)){ //here if the index is smalller than 0 or larger than 9
      return list; //it should return the original list
    } //the list should have 10 elements for its the original one, so it should differ from the newlist below which has 9 elements
    int newlist[] = new int[9]; //the new list have 9 elements
    int j = 0;
    for(int i = 0; i < 10; i++){ //we use the for loop to repeat 10 times so that we could run through the entire list
      if (i != pos){ //here we are comparing the element number to the position
        newlist[j] = list[i];
       // System.out.println(newlist[j] + " "); 
        j++;
      }
    }
    return newlist;
  }

   public static int [] remove(int [] list, int target){ // should create a new array that has one member fewer than list and be composed of all of the same members except the member in the position pos 
    int number = 0; 
    int j = 0;
    for(int i = 0; i < list.length; i++){ 
    if (list[i] == target) //we need to find the number of time target appears in the array list
      number++;
    }
    int otherlist[] = new int[10 - number]; //the new array should have 10 - number elements
    for(int i = 0; i < 10; i++){  //we use the for loop to repeat 10 times so that we could run through the entire list
      if (list[i] != target){ //here we are comparing the value of the element to the target
        otherlist[j] = list[i];
        //System.out.println(otherlist[j]);
        j++;
      }
    }
    return otherlist;
  }
   
  }//end of class
    

