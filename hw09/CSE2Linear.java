/* Yuhan Su
 * CSE 002 Section 210
 * November 27th 
 * */
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
  public static void main (String args[]){
    Scanner scan = new Scanner (System.in);
    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
    int [] array = new int[15];  
    int userEntered = 0;
    int i = 0;
    int prior = -100; //let prior be relatively small because array[-1] does not exist
    for (i = 0; i < array.length; i++){ //the array has 15 elements
      if(i != 0)
        prior = array[i - 1]; 
      if(!scan.hasNextInt()){ 
            System.out.println("Error in value");//first error
            return;
        }
      else{
        userEntered = scan.nextInt(); //let user enter 15 values
        if((userEntered < 0) || (userEntered > 100)){
            System.out.println("Error out of range");//second error
          return;
        }
        else if(userEntered < prior){
            System.out.println("Error comparing"); //third error
          return;
        }
        else{
            array[i] = userEntered;
            System.out.print(userEntered + " ");   
        }
      
      }
    }     
    System.out.println(" Enter a grade to search for:  ");
    int key = scan.nextInt(); //use the scanner here so that we can pass it directly to the binary search method
    BinarySearch(array, key);
    ScrambleArray(array);
    for(i = 0; i < array.length; i++){
      System.out.print(array[i] + " ");
    }
    System.out.println(" Enter a grade to search for:  ");
    int grade = scan.nextInt(); //we can pass it to the linear search method
    LinearSearch(array, grade);
    }

     public static void BinarySearch(int [] list, int searchKey){ //I declared the binary search method to find the entered grade
       int key = searchKey; //corresponding to the main method which prompts the user to enter a grade
          int low = 0;
          int high = list.length - 1;
          int mid = (low + high) / 2;
          int searchTime = 1; //let searchtime be 1 instead of 0 
          while((list[mid] != key)&&(high >= low)){
            if (list[mid] > key)
              high = mid - 1;
            else
              low = mid + 1;
              mid = (low + high) / 2;
              searchTime ++;
          }
          if (list[mid] == key)
              System.out.println(key + " " + "was found in the list with " + searchTime + " " + "iterations"); //print out the number of iterations used
          else
            System.out.println(key + " " + "was not found in the list with " + searchTime +" " + "iterations"); //print out the number of iterations used
          return; //return to main method
     }
     
    public static void ScrambleArray (int [] list){ //scramble the sorted array randomly, and print out the scrambled array
      for (int i = 0; i < 100; i++){ 
        Random randomGenerator = new Random(); //generate a random number
        int randomInt = randomGenerator.nextInt(list.length);
        while (randomInt == 0){
          randomInt = randomGenerator.nextInt(list.length);
        }
        int temp = list[0]; //swaps the element at that index with the first element 
        list[0] = list[randomInt];
        list[randomInt] = temp;
      }
      return; //return to main method
    }
      
   public static void LinearSearch(int [] list, int searchgrade){
     int i = 0;
     int grade = searchgrade; ////corresponding to the main method which prompts the user to enter a grade
     while((i < list.length)&&(list[i] != grade)){
       i++;
     }
     if (grade == list[i])
         System.out.println(grade + " " + "was found in " + (i + 1) + " " + "iterations"); //needs to be i plus one because it starts with one while i is zero
     else
         System.out.println(grade + " " + "was not found in " + (i + 1) + " " +"iterations");
      return; //return to main method
    }
   
  }//end of class