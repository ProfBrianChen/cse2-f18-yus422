/* Yuhan Su 
   CSE002 Section210 
   November 13th*/
import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 
public static void printArray(String [] list){ // takes an array of Strings and prints out each element, separated by a space
  for (int i = 0; i < list.length; i++){
    System.out.print(list[i] + " "); //print out the elements in the list array and combine each with space
  }
  System.out.println();
  return; 
  }

public static void shuffle(String [] list){ // shuffles the elements of the list by continuously randomize an index number of list
for (int i = 0; i < 52; i++){ 
  Random randomGenerator = new Random(); //generate a random number
  int randomInt = randomGenerator.nextInt(list.length);
  while (randomInt == 0){
    randomInt = randomGenerator.nextInt(list.length);
  }
      String temp = list[0]; //swaps the element at that index with the first element 
      list[0] = list[randomInt];
      list[randomInt] = temp;
}
  return;
}

public static String [] getHand(String [] list, int index, int numCards){ //returns an array that holds the number of cards specified in numCards
  String [] hand = new String [numCards];
  String[] suitNames={"C","H","S","D"};    
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
  String[] cards = new String[52]; 
  if (numCards > index + 1){ //If numCards is greater than the number of cards in the deck, create a new deck of cards
  for (int j = 0; j < 52; j++){ //create another deck of card
  cards[j] = rankNames[j % 13] + suitNames[j / 13]; 
}//end of for loop
  shuffle(cards);
  index = 51;
}//end of the if loop
  for (int i = numCards - 1; i >= 0; i--){ //Cards should be taken off at the end of the list of cards
    hand[i] = list[index - 4 + i];
}
  return hand; //return an array
}

public static void main(String[] args) { //main method
Scanner scan = new Scanner(System.in); //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i = 0; i < 52; i++){ 
  cards[i] = rankNames[i % 13] + suitNames[i / 13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   if(index < numCards) //relate to the loop in the third method,if the array was rebuilt in getHand method, index should be reset
     index = 51 - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
} //end of main method
} //end of class

