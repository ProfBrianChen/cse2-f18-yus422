/* Yuhan Su
   CSE002-210
   September 17th */

import java.util.Scanner;
public class Convert{
  //main method required for every java program
  public static void main (String[]args){
    Scanner myScanner = new Scanner( System.in ); //Import the Scanner class, and create a new instance called myScanner.
    
    System.out.print("Enter the affected areas in acres: ");// the number of acres of land affected by hurricane precipitation
    double affectedAreas = myScanner.nextDouble();
    
    System.out.print("Enter the rainfall in the affected area: ");//how many inches of rain were dropped
    
    double rainFallInArea = myScanner.nextDouble();
    
    
    double quantityOfRain;
    quantityOfRain = rainFallInArea/affectedAreas; //Convert the quantity of rain into cubic miles.
    
    System.out.println(quantityOfRain + " cubic miles");
    
  } //end of main method
} //end of class