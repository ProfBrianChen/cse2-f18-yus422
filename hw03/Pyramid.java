/* Yuhan Su
   CSE002-210
   September 17th */

import java.util.Scanner;
public class Pyramid{
  //main method required for every java program
  public static void main (String[]args){
    Scanner myScanner = new Scanner( System.in ); //Import the Scanner class, and create a new instance called myScanner
    
    System.out.print("The square side of the pyramid is (input length): ");// the length and width of the base are the same because it's a square
    double pyramidLength = myScanner.nextDouble();
    
    System.out.print("The height of the pyramid is (input height): ");//the height of the pyramid
    
    double pyramidHeight = myScanner.nextDouble();
    
    
    double pyramidVolume;
    pyramidVolume = pyramidLength * pyramidLength * pyramidHeight / 3; //the volume of the pyramid is one-third the area of the base times the height
    /* The cube root of double x can be computed as follows:
       double a = Math.pow(x, 1.0 / 3.0);
       The Math.pow() method computes x to the power of y, when written in the form 
       Math.pow(x,y);
    */
    
    System.out.println("The volume inside the pyramid is " + pyramidVolume);
    
  } //end of main method
} //end of class