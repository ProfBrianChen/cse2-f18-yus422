/* Yuhan Su 
   CSE002 Section210 
   October 10th*/



import java.util.Scanner;
public class PatternA{
  //main method required for every java program
  public static void main (String[]args){
    
    Scanner scr = new Scanner(System.in);
    
     System.out.println("Enter a integer between 1 and 10: ");  //Ask the user for a positive integer between 1 and 10 
    int length = scr.nextInt(); 
    String junk;

    
    while((length<=0)||(length>=11)&&(!scr.hasNextInt())){
      System.out.println("Error In Value");
      junk = scr.nextLine();
      length = scr.nextInt();
    }
    for(int numRows = 1; numRows <= length; numRows++){
   int numLine = 1;
     while (numLine <= numRows){
      System.out.print(numLine + " ");
      numLine++;
    }
     System.out.println("");
    }
  } //end of main method
} //end of class