/* Yuhan Su 
   CSE002 Section210 
   September 25th*/


/*
Randomly cast dice, generate two random numbers from 1-6, inclusive, representing the outcome of the two dice cast in the game.
Provide dice, use the Scanner twice to ask the user for two integers from 1-6, inclusive.  Assume the user always provides an integer, but check if the integer is within the range.
Determine the slang terminology of the outcome of the roll
Print out the slang terminology
Exit.

*/

import java.util.*;
public class CrapsIf{
  //main method required for every java program
  public static void main (String[]args){
	Scanner Melody = new Scanner ( System.in);
	System.out.println("1: random or 2: state the dice you want to evaluate?");
	int choice = Melody. nextInt();

	if(choice == 1){
		 int dice1 = (int)(Math.random()*6)+1;//generate a random number
      System.out.println(dice1);
    		int dice2 = (int)(Math.random()*6)+1;//generate a random number
      System.out.println(dice2);
    
     if((dice1 == 1)&&(dice2 == 1)){
      System.out.println("Snake Eyes");
    }
    if((dice1 == 1)&&(dice2 == 2)){
      System.out.println("Ace Deuce");
    }
    if((dice1 == 1)&&(dice2 == 3)){
      System.out.println("Easy Four");
    }
    if((dice1 == 1)&&(dice2 == 4)){
      System.out.println("Fever Five");
    }
    if((dice1 == 1)&&(dice2 == 5)){
      System.out.println("Easy Six");
    }
    if((dice1 == 1)&&(dice2 == 6)){
      System.out.println("Seven out");
    }
    if((dice1 == 2)&&(dice2 == 1)){
      System.out.println("Ace Deuce");
    }
    if((dice1 == 2)&&(dice2 == 2)){
      System.out.println("Hard four");
    }
    if((dice1 == 2)&&(dice2 == 3)){
      System.out.println("Fever five");
    }
    if((dice1 == 2)&&(dice2 == 4)){
      System.out.println("Easy six");
    }
    if((dice1 == 2)&&(dice2 == 5)){
      System.out.println("Seven out");
    }
    if((dice1 == 2)&&(dice2 == 6)){
      System.out.println("Easy Eight");
    }
     if((dice1 == 3)&&(dice2 == 1)){
      System.out.println("Easy Four");
    }
    if((dice1 == 3)&&(dice2 == 2)){
      System.out.println("Fever five");
    }
    if((dice1 == 3)&&(dice2 == 3)){
      System.out.println("Hard six");
    }
    if((dice1 == 3)&&(dice2 == 4)){
      System.out.println("Seven out");
    }
    if((dice1 == 3)&&(dice2 == 5)){
      System.out.println("Easy Eight");
    }
    if((dice1 == 3)&&(dice2 == 6)){
      System.out.println("Nine");
    }
    if((dice1 == 4)&&(dice2 == 1)){
      System.out.println("Fever Five");
    }
    if((dice1 == 4)&&(dice2 == 2)){
      System.out.println("Easy six");
    }
    if((dice1 == 4)&&(dice2 == 3)){
      System.out.println("Seven out");
    }
    if((dice1 == 4)&&(dice2 == 4)){
      System.out.println("Hard Eight");
    }
    if((dice1 == 4)&&(dice2 == 5)){
      System.out.println("Nine");
    }
    if((dice1 == 4)&&(dice2 == 6)){
      System.out.println("Easy Ten");
    }
    if((dice1 == 5)&&(dice2 == 1)){
      System.out.println("Easy Six");
    }
    if((dice1 == 5)&&(dice2 == 2)){
      System.out.println("Seven out");
    }
    if((dice1 == 5)&&(dice2 == 3)){
      System.out.println("Easy Eight");
    }
    if((dice1 == 5)&&(dice2 == 4)){
      System.out.println("Nine");
    }
    if((dice1 == 5)&&(dice2 == 5)){
      System.out.println("Hard Ten");
    }
    if((dice1 == 5)&&(dice2 == 6)){
      System.out.println("Yo-leven");
    }
    if((dice1 == 6)&&(dice2 == 1)){
      System.out.println("Seven out");
    }
    if((dice1 == 6)&&(dice2 == 2)){
      System.out.println("Easy Eight");
    }
    if((dice1 == 6)&&(dice2 == 3)){
      System.out.println("Nine");
    }
    if((dice1 == 6)&&(dice2 == 4)){
      System.out.println("Easy Ten");
    }
    if((dice1 == 6)&&(dice2 == 5)){
      System.out.println("Yo-leven");
    }
    if((dice1 == 6)&&(dice2 == 6)){
      System.out.println("Boxcars");
    }
	 
	}
   
   else{ //if the user picks the second option which is to state the two numbers
    Scanner myScanner = new Scanner( System.in );
    System.out.println("Enter the number of dice 1 (from 1 to 6): ");
    int dice1 = myScanner.nextInt();
    System.out.println("Enter the number of dice 2 (from 1 to 6): ");
    int dice2 = myScanner.nextInt();

    
    if((dice1 == 1)&&(dice2 == 1)){
      System.out.println("Snake Eyes");
    }
    if((dice1 == 1)&&(dice2 == 2)){
      System.out.println("Ace Deuce");
    }
    if((dice1 == 1)&&(dice2 == 3)){
      System.out.println("Easy Four");
    }
    if((dice1 == 1)&&(dice2 == 4)){
      System.out.println("Fever Five");
    }
    if((dice1 == 1)&&(dice2 == 5)){
      System.out.println("Easy Six");
    }
    if((dice1 == 1)&&(dice2 == 6)){
      System.out.println("Seven out");
    }
    if((dice1 == 2)&&(dice2 == 1)){
      System.out.println("Ace Deuce");
    }
    if((dice1 == 2)&&(dice2 == 2)){
      System.out.println("Hard four");
    }
    if((dice1 == 2)&&(dice2 == 3)){
      System.out.println("Fever five");
    }
    if((dice1 == 2)&&(dice2 == 4)){
      System.out.println("Easy six");
    }
    if((dice1 == 2)&&(dice2 == 5)){
      System.out.println("Seven out");
    }
    if((dice1 == 2)&&(dice2 == 6)){
      System.out.println("Easy Eight");
    }
     if((dice1 == 3)&&(dice2 == 1)){
      System.out.println("Easy Four");
    }
    if((dice1 == 3)&&(dice2 == 2)){
      System.out.println("Fever five");
    }
    if((dice1 == 3)&&(dice2 == 3)){
      System.out.println("Hard six");
    }
    if((dice1 == 3)&&(dice2 == 4)){
      System.out.println("Seven out");
    }
    if((dice1 == 3)&&(dice2 == 5)){
      System.out.println("Easy Eight");
    }
    if((dice1 == 3)&&(dice2 == 6)){
      System.out.println("Nine");
    }
    if((dice1 == 4)&&(dice2 == 1)){
      System.out.println("Fever Five");
    }
    if((dice1 == 4)&&(dice2 == 2)){
      System.out.println("Easy six");
    }
    if((dice1 == 4)&&(dice2 == 3)){
      System.out.println("Seven out");
    }
    if((dice1 == 4)&&(dice2 == 4)){
      System.out.println("Hard Eight");
    }
    if((dice1 == 4)&&(dice2 == 5)){
      System.out.println("Nine");
    }
    if((dice1 == 4)&&(dice2 == 6)){
      System.out.println("Easy Ten");
    }
    if((dice1 == 5)&&(dice2 == 1)){
      System.out.println("Easy Six");
    }
    if((dice1 == 5)&&(dice2 == 2)){
      System.out.println("Seven out");
    }
    if((dice1 == 5)&&(dice2 == 3)){
      System.out.println("Easy Eight");
    }
    if((dice1 == 5)&&(dice2 == 4)){
      System.out.println("Nine");
    }
    if((dice1 == 5)&&(dice2 == 5)){
      System.out.println("Hard Ten");
    }
    if((dice1 == 5)&&(dice2 == 6)){
      System.out.println("Yo-leven");
    }
    if((dice1 == 6)&&(dice2 == 1)){
      System.out.println("Seven out");
    }
    if((dice1 == 6)&&(dice2 == 2)){
      System.out.println("Easy Eight");
    }
    if((dice1 == 6)&&(dice2 == 3)){
      System.out.println("Nine");
    }
    if((dice1 == 6)&&(dice2 == 4)){
      System.out.println("Easy Ten");
    }
    if((dice1 == 6)&&(dice2 == 5)){
      System.out.println("Yo-leven");
    }
    if((dice1 == 6)&&(dice2 == 6)){
      System.out.println("Boxcars");
    }
   }
  } //end of main method
} //end of class